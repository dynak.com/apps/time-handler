import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  maxWorkers: 0,
  rootDir: './',
  transform: { '^.+\\.ts?$': 'ts-jest' },
  testEnvironment: 'node',
  testTimeout: 20000,
  testRegex: '^.+\\.(test|spec)?\\.(ts|tsx)$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  globals: {
    'ts-jest': {
      tsconfig: 'tsconfig.json',
    },
  },
};

export default config;
