#!/usr/bin/env node
import { authorize, connectCalendar } from './auth';
import { setConfig, argv } from './config';
import { select } from './utils/select';
import { handleTime } from './timeHandler';
import { datePicker } from './datePicker';

const config = setConfig();

const init = async () => {
  const users = (await select(argv.user, config.USERS, 'Set users'))
    .split(',')
    .map((user) => {
      const userArr = user.split(/ as /i);
      return {
        calendarID: userArr[0]?.trim(),
        alias: userArr[1]?.trim() || userArr[0].trim()
      };
    });

  const date = await datePicker();
  const hourlyRate = await select(
    argv.hourly_rate?.toString(),
    config.HOURLY_RATE,
    'Set hourly rate'
  );
  const credentials = {
    clientID: await select(argv.client_id, config.CLIENT_ID, 'Set client_id'),
    clientSecret: await select(
      argv.client_secret,
      config.CLIENT_SECRET,
      'Set client_secret'
    ),
    redirectURI: await select(
      argv.redirect_uri,
      config.REDIRECT_URI,
      'Set redirect_uri'
    )
  };

  await authorize(credentials, async (token) => {
    await connectCalendar(
      credentials,
      token,
      users,
      date,
      isNaN(Number(hourlyRate)) ? 1 : Number(hourlyRate),
      handleTime
    );
  });
};

init();
