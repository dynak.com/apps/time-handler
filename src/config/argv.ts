import yargs from 'yargs';

export const argv = yargs
  .options({
    token: {
      type: 'string',
      alias: ['T', 't'],
      describe: 'Name of token json file',
    },
    client_id: { type: 'string', describe: 'Google API client ID' },
    client_secret: { type: 'string', describe: 'Google API client secret' },
    redirect_uri: { type: 'string', describe: 'Google API redirect URI' },

    hourly_rate: {
      type: 'number',
      describe:
        'Hourly rate (if hourly rate is not intiger use . instead of ,)',
    },
    start: { type: 'string', alias: ['S', 's'], describe: 'Start date' },
    end: { type: 'string', alias: ['E', 'e'], describe: 'End date' },
    user: {
      type: 'string',
      alias: ['U', 'users', 'u'],
      describe:
        'Calendar ID (mostly email address), optionally you can set alias f.e. `user1@example.com as User One`. If you have many users use coma as separator.',
    },
    this_month: {
      type: 'boolean',
      default: false,
      describe: 'Set date range to this month (overwrite others dates)',
    },
    verbose: {
      type: 'boolean',
      default: false,
      describe: 'Print events summary',
    },
  })
  .help('help')
  .alias('help', 'h')
  .alias('version', 'v')
  .example([
    [
      `-u "user1@example.com as User One, user2@example.com"`,
      '2 users, user1@example.com user2@example.com, first one will have alias',
    ],
    [
      `-s 2021-04-09 --end 2021-04-10`,
      `Date range: 2021-04-09 00:00:00 - 2021-04-10 00:00:00`,
    ],
  ]).argv;
