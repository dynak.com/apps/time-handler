import dotenv from 'dotenv';
import Joi from 'joi';
import shell from 'shelljs';
import fs from 'fs';
import path from 'path';
import os from 'os';
import { ConfigType } from '../common/types';
import { logs } from '../utils';
import { checkIfconfigIsConfigType } from '../common/typeguards';

export const getConfigPath = (fileName?: string): string => {
  const globalDir = path.join(os.homedir(), '.local', 'share', '.time-handler');

  if (!fs.existsSync(globalDir)) shell.mkdir('-p', globalDir);

  return fileName ? path.join(globalDir, fileName) : globalDir;
};

export const setConfig = (): ConfigType => {
  const envPath = getConfigPath('.env');
  if (!fs.existsSync(envPath)) fs.writeFileSync(envPath, '');

  const cfg = dotenv.config({ path: envPath }).parsed || {};

  const configSchema = Joi.object({
    TOKEN: Joi.string().default('token'),
    CLIENT_ID: Joi.string().default(''),
    CLIENT_SECRET: Joi.string().default(''),
    REDIRECT_URI: Joi.string().default(''),

    HOURLY_RATE: Joi.string().default(''),
    USERS: Joi.string().default(''),
    START: Joi.string().default(''),
    END: Joi.string().default(''),
    THIS_MONTH: Joi.boolean().default(false),
    VERBOSE: Joi.boolean().default(false),
  });

  const config = configSchema.validate(cfg);
  const processEnvProps = Object.keys(config.value);

  processEnvProps.forEach((prop) => {
    if (process.env[prop] === undefined) {
      process.env[prop] = config.value[prop];
    }
  });

  if (config.error) {
    config.error.details.forEach(({ message }) => {
      logs.configErrorLog(message);
    });
    process.exit(1);
  }

  const configFromEnv = process.env;
  if (!checkIfconfigIsConfigType(configFromEnv)) {
    throw new Error('Incorrect config!');
  }

  return configFromEnv;
};
