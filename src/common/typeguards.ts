import { ConfigType, EventType } from './types';
import { calendar_v3 } from 'googleapis';

export const checkIfconfigIsConfigType = (
  config: Object | ConfigType
): config is ConfigType => {
  const configModel = config as ConfigType;
  return (
    typeof configModel.TOKEN === 'string' &&
    typeof configModel.CLIENT_ID === 'string' &&
    typeof configModel.CLIENT_SECRET === 'string' &&
    typeof configModel.REDIRECT_URI === 'string' &&
    typeof configModel.HOURLY_RATE === 'string' &&
    typeof configModel.USERS === 'string' &&
    typeof configModel.START === 'string' &&
    typeof configModel.END === 'string' &&
    typeof configModel.THIS_MONTH === 'string' &&
    typeof configModel.VERBOSE === 'string'
  );
};

export const checkIfEventIsEventType = (
  event: EventType | calendar_v3.Schema$Event
): event is EventType => {
  const EventModel = event as EventType;
  return (
    EventModel.start.dateTime !== undefined &&
    EventModel.end.dateTime !== undefined &&
    typeof EventModel.eventType === 'string'
  );
};
