import { OAuth2Client } from 'google-auth-library';
import { DateObjectType, UserType, CredentialsType } from './types';
import { calendar_v3 } from 'googleapis';
import { GaxiosResponse } from 'gaxios';

interface AuthCallbackInterface {
  (token: string): Promise<void>;
}

export interface AuthInterface {
  (
    credentials: CredentialsType,
    callback: AuthCallbackInterface
  ): Promise<void>;
}
export interface GetAccessTokenInterface {
  (credentials: CredentialsType, callback: AuthCallbackInterface): void;
}

export interface GetCredentialsInterface {
  (auth: AuthInterface, callback: AuthCallbackInterface): Promise<void>;
}

export interface ConnectCalendarInterface {
  (
    credentials: CredentialsType,
    token: string,
    users: UserType[],
    date: DateObjectType,
    hourlyRate: number,
    callback: HandleTimeInterface
  ): Promise<void>;
}

export interface HandleTimeInterface {
  (
    res: GaxiosResponse<calendar_v3.Schema$Events>,
    user: UserType,
    date: DateObjectType,
    hourlyRate: number
  ): { user: string; totalTime: number };
}

export interface CountEventDurationInterface {
  (
    eventStartTime: number,
    eventEndTime: number,
    lastEndedEventEndTime: number
  ): number;
}

export interface GetResponseStatusInterface {
  (
    attendees: {
      email: string;
      responseStatus: string;
    }[],
    user: string
  ): string;
}

export interface CheckIfEventDurationShouldBeCountedInterface {
  (responseStatus: string, eventType: string, selfCreated: boolean): boolean;
}
