export type ConfigType = {
  TOKEN: string;

  CLIENT_ID: string;
  CLIENT_SECRET: string;
  REDIRECT_URI: string;

  HOURLY_RATE: string;
  USERS: string;
  START: string;
  END: string;
  THIS_MONTH: string;
  VERBOSE: string;
};

export type DateObjectType = {
  start: Date;
  end: Date;
};

export type EventType = {
  summary: string;
  start: {
    dateTime: string;
  };
  end: {
    dateTime: string;
  };
  attendees: [{ email: string; responseStatus: string }];
  eventType: string;
};

export type CredntialsType = {
  clientID: string;
  clientSecret: string;
  redirectURI: string;
};

export type UserType = { calendarID: string; alias: string };

export type CredentialsType = {
  clientID: string;
  clientSecret: string;
  redirectURI: string;
};
