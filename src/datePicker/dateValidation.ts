export const dateValidation = (date: Date | undefined) => {
  if (date && date instanceof Date && !isNaN(date.getTime())) {
    return date;
  } else {
    console.log('Incorrect date!');
    process.exit(1);
  }
};
