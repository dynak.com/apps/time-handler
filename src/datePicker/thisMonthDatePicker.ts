import { dateValidation } from './dateValidation';
import { DateObjectType } from '../common/types';

export const thisMonthDatePicker = (): DateObjectType => {
  const currentDate = new Date();
  return {
    start: dateValidation(
      new Date(`${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-01`)
    ),
    end: dateValidation(
      new Date(
        currentDate.getMonth() !== 11
          ? `${currentDate.getFullYear()}-${currentDate.getMonth() + 2}-01`
          : `${currentDate.getFullYear() + 1}-01-01`
      )
    ),
  };
};
