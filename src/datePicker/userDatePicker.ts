import { argv, setConfig } from '../config';
import { select } from '../utils/select';
import { dateValidation } from './dateValidation';
import { DateObjectType } from '../common/types';

const config = setConfig();

export const userDatePicker = async (): Promise<DateObjectType> => {
  const dateRange = {
    start: dateValidation(
      new Date(await select(argv.start, config.START, 'Set start date'))
    ),
    end: dateValidation(
      new Date(await select(argv.end, config.END, 'Set end date'))
    )
  };
  if (dateRange.start > dateRange.end) {
    console.error('Incorrect date range!');
    process.exit();
  }
  return dateRange;
};
