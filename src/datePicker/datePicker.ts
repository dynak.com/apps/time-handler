import { argv, setConfig } from '../config';
import { thisMonthDatePicker } from './thisMonthDatePicker';
import { userDatePicker } from './userDatePicker';
import { DateObjectType } from '../common/types';

const config = setConfig();

export const datePicker = async (): Promise<DateObjectType> => {
  if (argv.this_month || config.THIS_MONTH === 'true') {
    return thisMonthDatePicker();
  } else {
    return await userDatePicker();
  }
};
