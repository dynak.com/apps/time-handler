export const configErrorLog = (message: string): void => {
  console.error(`Configuration error: ${message}!`);
};

export const totalTimeLog = (
  user: string,
  totalTime: number,
  hourlyRate: number
): void => {
  console.log(
    `${user} totalTime: ${totalTime} min, ${Math.floor(totalTime / 60)} h,  ${
      totalTime % 60
    } min${
      hourlyRate !== 1
        ? `, ${((totalTime * hourlyRate) / 60).toFixed(2)} pln`
        : ''
    }`
  );
};
