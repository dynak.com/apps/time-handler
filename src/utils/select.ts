import { input } from './input';

export const select = async (
  arg: string | undefined,
  envVar: string | undefined,
  question: string
): Promise<string> => {
  if (arg) {
    return arg;
  } else if (envVar) {
    return envVar;
  } else {
    return await input(`${question}: `);
  }
};
