import readline from 'readline';

export const input = (text: string): Promise<string> => {
  return new Promise((resolve) => {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    rl.question(text, (answer) => {
      resolve(answer.trim());
      rl.close();
    });
  });
};
