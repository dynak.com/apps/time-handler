import { promisify } from 'util';
import fs from 'fs';
import { getAccessToken } from './getAccessToken';
import { AuthInterface } from '../common/interfaces';
import { setConfig, getConfigPath, argv } from '../config';

const config = setConfig();

export const authorize: AuthInterface = async (credentials, callback) => {
  const asyncReadFile = promisify(fs.readFile);
  const token = await asyncReadFile(
    getConfigPath(`${argv.token || config.TOKEN}.json`),
    { encoding: 'utf-8' }
  ).catch(async (err) => await getAccessToken(credentials, callback));
  if (typeof token === 'string') {
    await callback(token);
  }
};
