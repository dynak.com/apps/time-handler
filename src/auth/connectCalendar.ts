import { google } from 'googleapis';
import { promisify } from 'util';
import fs from 'fs';
import { ConnectCalendarInterface } from '../common/interfaces';
import { setConfig, getConfigPath, argv } from '../config';

const config = setConfig();

export const connectCalendar: ConnectCalendarInterface = async (
  credentials,
  tokenStr,
  users,
  date,
  hourlyRate,
  handleTime
) => {
  const asyncWriteFile = promisify(fs.writeFile);

  const oAuth2Client = new google.auth.OAuth2(
    credentials.clientID,
    credentials.clientSecret,
    credentials.redirectURI
  );

  oAuth2Client.setCredentials(JSON.parse(tokenStr));

  const calendar = google.calendar({ version: 'v3', auth: oAuth2Client });
  users.forEach((user) =>
    calendar.events.list(
      {
        calendarId: user.calendarID,
        timeMin: date.start.toISOString(),
        timeMax: date.end.toISOString(),
        singleEvents: true,
        orderBy: 'startTime'
      },
      (err, res) => {
        if (err) {
          if (err.message === `invalid_request`) {
            oAuth2Client.refreshAccessToken(async (err, newToken) => {
              if (err) return console.log(err.message);
              const tokenPath = getConfigPath(
                `${argv.token || config.TOKEN}.json`
              );
              await asyncWriteFile(tokenPath, JSON.stringify(newToken))
                .catch((err) => {
                  console.error(err);
                })
                .then(async () => {
                  await connectCalendar(
                    credentials,
                    tokenStr,
                    users,
                    date,
                    hourlyRate,
                    handleTime
                  );
                });
            });
          } else {
            return console.log(
              `${user.alias} The API returned an error: ${err}`
            );
          }
        } else {
          if (!res) return console.log('API not response!');
          handleTime(res, user, date, hourlyRate);
        }
      }
    )
  );
};
