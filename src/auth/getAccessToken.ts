import readline from 'readline';
import fs from 'fs';
import { promisify } from 'util';
import { google } from 'googleapis';
import { GetAccessTokenInterface } from '../common/interfaces';
import { setConfig, getConfigPath, argv } from '../config';

const config = setConfig();

export const getAccessToken: GetAccessTokenInterface = async (
  credentials,
  callback
) => {
  const asyncWriteFile = promisify(fs.writeFile);
  const tokenPath = getConfigPath(`${argv.token || config.TOKEN}.json`);

  const oAuth2Client = new google.auth.OAuth2(
    credentials.clientID,
    credentials.clientSecret,
    credentials.redirectURI
  );

  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: ['https://www.googleapis.com/auth/calendar.readonly']
  });

  console.log('Authorize this app by visiting this url:', authUrl);

  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

  try {
    rl.question(`Enter the code from that page here: `, async (code) => {
      rl.close();
      oAuth2Client.getToken(code, async (err, token) => {
        if (err || !token)
          return console.error('Error retrieving access token', err);
        oAuth2Client.setCredentials(token);

        const tokenStr = JSON.stringify(token);
        await asyncWriteFile(tokenPath, tokenStr)
          .catch((err) => {
            console.error(err);
          })
          .then(async () => {
            await callback(tokenStr);
          });
      });
    });
  } catch (e) {
    console.error(e);
  } finally {
    return 1;
  }
};
