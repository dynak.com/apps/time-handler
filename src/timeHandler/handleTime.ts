import { HandleTimeInterface } from '../common/interfaces';
import { checkIfEventIsEventType } from '../common/typeguards';
import { logs } from '../utils';
import { countEventDuration } from './countEventDuration';
import { checkIfEventDurationShouldBeCounted } from './checkIfEventDurationShouldBeCounted';
import { getResponseStatus } from './getResponseStatus';
import { setConfig, argv } from '../config';

const config = setConfig();

export const handleTime: HandleTimeInterface = (
  res,
  user,
  date,
  hourlyRate
) => {
  let totalTime = 0;
  let lastEndedEventEndTime = date.start.getTime();
  const events = res.data.items;

  if (events) {
    events.map((event) => {
      if (checkIfEventIsEventType(event)) {
        const { attendees, start, end, eventType } = event;
        const selfCreated = !attendees;
        const responseStatus = getResponseStatus(attendees, user.calendarID);

        const eventStartTime = new Date(start.dateTime).getTime();
        const eventEndTime = new Date(end.dateTime).getTime();

        const duration = countEventDuration(
          eventStartTime,
          eventEndTime,
          lastEndedEventEndTime
        );

        if (
          checkIfEventDurationShouldBeCounted(
            responseStatus,
            eventType,
            selfCreated
          )
        ) {
          if (argv.verbose || config.VERBOSE === 'true') {
            console.log(`* ${event.summary}`);
          }

          lastEndedEventEndTime =
            eventEndTime > lastEndedEventEndTime
              ? eventEndTime
              : lastEndedEventEndTime;
          totalTime += duration;
        }
      }
    });
  }
  logs.totalTimeLog(user.alias, totalTime, hourlyRate);
  return { user: user.alias, totalTime };
};
