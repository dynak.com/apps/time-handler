export { checkIfEventDurationShouldBeCounted } from './checkIfEventDurationShouldBeCounted';
export { countEventDuration } from './countEventDuration';
export { getResponseStatus } from './getResponseStatus';
export { handleTime } from './handleTime';
