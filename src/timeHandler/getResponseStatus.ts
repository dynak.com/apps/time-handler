import { GetResponseStatusInterface } from '../common/interfaces';

export const getResponseStatus: GetResponseStatusInterface = (
  attendees,
  user
) => {
  if (attendees) {
    const responseStatus = attendees
      .map((attendee) => {
        if (attendee.email === user) {
          return attendee.responseStatus;
        }
      })
      .join('');
    return responseStatus;
  }
  return 'unknown';
};
