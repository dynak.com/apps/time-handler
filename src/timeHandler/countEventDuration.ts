import { CountEventDurationInterface } from '../common/interfaces';

export const countEventDuration: CountEventDurationInterface = (
  eventStartTime,
  eventEndTime,
  lastEndedEventEndTime
) => {
  if (eventStartTime >= lastEndedEventEndTime) {
    return (eventEndTime - eventStartTime) / 60000;
  } else if (eventEndTime > lastEndedEventEndTime) {
    return (eventEndTime - lastEndedEventEndTime) / 60000;
  } else {
    return 0;
  }
};
