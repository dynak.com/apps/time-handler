import { CheckIfEventDurationShouldBeCountedInterface } from '../common/interfaces';
import { EventTypesEnum } from '../common/enums';

export const checkIfEventDurationShouldBeCounted: CheckIfEventDurationShouldBeCountedInterface = (
  responseStatus,
  eventType,
  selfCreated
) => {
  return (
    (responseStatus === 'accepted' || selfCreated) &&
    eventType !== EventTypesEnum.outOfOffice
  );
};
