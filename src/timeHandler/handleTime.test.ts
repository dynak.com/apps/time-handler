import { handleTime } from './handleTime';

const data = {
  data: {
    items: [
      {
        summary: 'event1',
        start: { dateTime: '2020-10-01T09:30:00+02:00' },
        end: { dateTime: '2020-10-01T10:30:00+02:00' },
        eventType: 'default',
      },
      {
        summary: 'inside event1',
        start: { dateTime: '2020-10-01T09:45:00+02:00' },
        end: {
          dateTime: '2020-10-01T10:00:00+02:00',
          attendees: [
            {
              email: 'user1@example.com',
              responseStatus: 'accepted',
            },
            {
              email: 'user2@example.com',
              responseStatus: 'accepted',
            },
            {
              email: 'user3@example.com',
              responseStatus: 'accepted',
            },
          ],
        },
        eventType: 'default',
      },
      {
        summary: 'event2',
        start: { dateTime: '2020-10-01T10:30:00+02:00' },
        end: { dateTime: '2020-10-01T12:00:00+02:00' },
        attendees: [
          {
            email: 'user1@example.com',
            responseStatus: 'accepted',
          },
          {
            email: 'user2@example.com',
            responseStatus: 'accepted',
          },
          {
            email: 'user3@example.com',
            responseStatus: 'accepted',
          },
        ],
        eventType: 'default',
      },
      {
        summary: 'patrly in event2',
        start: { dateTime: '2020-10-01T11:30:00+02:00' },
        end: { dateTime: '2020-10-01T12:30:00+02:00' },
        eventType: 'default',
      },
      {
        summary: 'event3',
        start: { dateTime: '2020-10-01T13:30:00+02:00' },
        end: { dateTime: '2020-10-01T14:00:00+02:00' },
        eventType: 'default',
      },
      {
        summary: 'needsAction event',
        start: { dateTime: '2020-10-01T14:30:00+02:00' },
        end: { dateTime: '2020-10-01T15:00:00+02:00' },
        attendees: [
          {
            email: 'user1@example.com',
            responseStatus: 'needsAction',
          },
          {
            email: 'user2@example.com',
            responseStatus: 'accepted',
          },
        ],
        eventType: 'default',
      },
      {
        summary: 'declined event',
        start: { dateTime: '2020-10-01T15:30:00+02:00' },
        end: { dateTime: '2020-10-01T16:00:00+02:00' },
        attendees: [
          {
            email: 'user1@example.com',
            responseStatus: 'declined',
          },
          {
            email: 'user2@example.com',
            responseStatus: 'accepted',
          },
        ],
        eventType: 'default',
      },
      {
        summary: 'tentative event',
        start: { dateTime: '2020-10-01T16:30:00+02:00' },
        end: { dateTime: '2020-10-01T17:00:00+02:00' },
        attendees: [
          {
            email: 'user1@example.com',
            responseStatus: 'tentative',
          },
          {
            email: 'user2@example.com',
            responseStatus: 'accepted',
          },
        ],
        eventType: 'default',
      },
      {
        summary: 'Out of office',
        start: { dateTime: '2020-10-01T18:00:00+02:00' },
        end: { dateTime: '2020-10-01T20:00:00+02:00' },
        eventType: 'outOfOffice',
      },
    ],
  },
  config: {},
  headers: {},
  request: {
    responseURL: '',
  },
  status: 200,
  statusText: 'OK',
};

test('Return correct totalTime', () => {
  const dateObject = {
    start: new Date('2020-10-01'),
    end: new Date('2020-10-02'),
  };

  const message = handleTime(
    data,
    { calendarID: 'user1@example.com', alias: 'User1' },
    dateObject,
    2.5
  );

  expect(message).toMatchObject({ user: 'User1', totalTime: 210 });
});
